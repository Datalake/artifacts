# S3 Úložiště

- [Struktura úložiště](#struktura-úložiště)
- [Připojení k S3 úložišti](#připojení-k-úložšti)
- [Bucket policy](#bucket-policy)
- [Lifecycle policy](#lifecycle-policy)
- [Manipulace s daty](#manipulace-s-daty)

Základními prvky S3 úložiště jsou buckety a objekty. Bucket je kontejner, který může obsahovat množinu objektů. Objekty jsou v rámci bucketu ukládány ve stejné úrovni, tj. úložiště není hierarchické. Hierarchická struktura je však simulována pomocí prefixů. Přístup uživatelů k datům lze řídit buď na úrovni bucketů, nebo na úrovni prefixů. Nastavení probíhá pomocí tzv. bucket policy.

**Limity**

- Maximální počet bucketů: 1000

- Doporučený maximální počet souborů na bucket CESNETem: 1M (při vyšších počtech může docházet ke zpomalení úložiště)

## Struktura úložiště

### Tiers

Data jsou v úložišti rozdělená do úrovní (tiers) podle úrovně zpracování. Na nejnižší úrovni jsou data v takové podobě jak se sbírají, každá další úroveň potom zvyšuje míru zpracování, čistotu, úroveň filtrace dat atd. Zároveň se s každou úrovní zvyšuje pravděpodobnost výskytu chyby a proto je důležité dlouhodobě uchovávat i data na nejnižší úrovni. Tohle rozdělení je jedním z mechanismů pro zamezení vzniku data swamp. Navržené úrovně jsou následující:

- TIER 0 RAW: Nezpracovaná data, bez jakéhokoli předzpracování a metadat.

- TIER 1 STANDARD: Data ve strukturovaném a standardizovaném formátu, řídí se Elastic Common Schema. Výstup z Beats.

- TIER 2 CURATED: Zpracovaná data která byla rozparsována (např. logstashem) a nabízí vysokou flexibilitu pro analýzu.

Další úrovně budou definovány v další fázi projektu podle individuálních potřeb. Očekávané úrovně jsou: PLAYGROUND pro experimenty, ARCHIVAL pro agregovaná archivní data.

### Buckets

- Jeden bucket pro každou úroveň + jeden bucket pro testování

- Bucket `s3://playground/` je k dispozici pro testovací účely všem uživatelům. Měla by se do něj nahrávat pouze testovací data!

- TIER 0 data bucket: `s3://data-raw/`

- TIER 1 data bucket: `s3://data-standard/`

- TIER 2 data bucket: `s3://data-curated/`

### Prefixy

- Každý objekt má následující prefix (podle použitého klienta):

  - Winlogbeat: `group={group_name}/host={host_name}/channel={wineventlog_channel}/year={yyyy}/month={MM}/day={dd}/hour={HH}`.

  - Filebeat: `group={group_name}/host={host_name}/year={+yyyy}/month={+MM}/day={+dd}/hour={+HH}` (je vynechána položka `channel`).

- Prefixy jsou použity k řízení přístupu uživatelů k objektům v bucket. Konkrétně podle hodnoty `group_name`. Každému uživateli (respektive skupině) jsou přiděleny přístupové údaje (access key a secret key), které umožňují číst objekty, které mají v prefixu příslušnou hodnotu `group_name`.

### Objekty

- Logy jsou seskupeny a uloženy v textových souborech komprimovaných do formátu gzip (= S3 objekty).

- Formát dat v souboru: json lines

- Soubor bývají většinou nahrány do S3 na základě nějaké podmínky. Typicky se jedná o maximální velikost a maximální časový interval.

- Příklad jednoho záznamu json ze systému filebeat:

    ```json
    {
        "input":
        {
            "type": "filestream"
        },
        "ecs":
        {
            "version": "8.0.0"
        },
        "message": "May 23 07:00:23 my-strange-hostname logged a message",
        "host":
        {
            "name": "my-strange-hostname"
        },
        "log":
        {
            "file":
            {
                "path": "/var/log/path-to-strange-log.log"
            },
            "offset": 157888
        },
        "tags":
        [
            "beats_input_codec_plain_applied"
        ],
        "@version": "1",
        "event":
        {
            "original": "May 23 07:00:23 my-strange-hostname logged a message"
        },
        "@timestamp": "2023-05-23T05:00:29.586Z",
        "agent":
        {
            "version": "8.7.1",
            "type": "filebeat",
            "name": "my-strange-hostname",
            "id": "00000000-1111-tttt-bbca-xxxxxxxxcccc",
            "ephemeral_id": "00000000-ruru-444e-b211-eerreerreerr"
        },
        "fields":
        {
            "group": "my-strange-group-name",
            "bare_hostname": "my-strange-hostname"
        }
    }
    ```

### Šifrování

- Server side šifrování není podporováno

- Možnost použití client side šifrování. Ideálně nástroj [rclone](https://du.cesnet.cz/cs/navody/object_storage/rclone/rclone-extra-encryption/start), nebo [cipher plugin pro Logstash](https://www.elastic.co/guide/en/logstash/current/plugins-filters-cipher.html).

## Připojení k úložšti

### Přístupové údaje k úložišti

Uživatelský účet k S3 úložišti se skládá ze tří částí.

- `user` ve formátu `{storage_id}${user_id}`: id pro nastavení politik

- `access_key`: login pro připojení k úložišti

- `secret_key`: klíč pro připojení k úložišti

Tyto údaje budou přiděleny CESNETem. Před jejich vyžádáním je potřeba nejprve vytvořit servisní identity v systému Perun. Na každou servisní identitu lze navázat jedny S3 přístupové údaje. Návod pro vytvoření servisní identity lze nalézt na stránkách [CESNETu](https://du.cesnet.cz/cs/navody/perun/vytvoreni_servisniho_uctu/start).

### Klienti

Přehled možných klientů pro připojení k S3 úložišti lze nalézt v dokumentaci [CESNETu](https://du.cesnet.cz/cs/navody/object_storage/cesnet_s3/start). Pro potřeby nastavení bucket politik, bude potřeba mít nainstalovaný klient [AWS CLI](https://du.cesnet.cz/cs/navody/object_storage/awscli/start).

### Doporučené rozvržení uživatelských účtů

- Admin pro CRUD všeho.

- User účty pro přístup ke konkrétním prefixům, ideálně pouze pro čtení. Do úložiště bude data ukládat nějaký nástroj (např. logstash), nikoliv přímo uživatel.

- Testovací účet s přístupem pouze k testovacímu bucketu.

## Bucket policy

Bucket policy je json soubor, kterým lze řídit přístup uživatelů k objektům v bucketu. Bucket policies jsou tzv. resource-based, což znamená, že se aplikují přímo na bucket a jeho objekty, nikoli na uživatele.

### Vypsání aplikované bucket policy

```shell
aws s3api get-bucket-policy --endpoint-url https://s3.clX.du.cesnet.cz --bucket {bucket_name}
```

### Aplikace bucket policy

```shell
aws s3api put-bucket-policy --endpoint-url https://s3.clX.du.cesnet.cz --bucket {bucket_name} --policy file://{path_to_policy}
```

**POZOR:** Aplikace politiky zcela přepíše předchozí. Nejedná se tedy o inkrementální změnu, ale nahrazení.

### Příklad

Nastavení bucket policy bude demonstrováno na následujícím příkladě:

- V S3 je bucket `main`

- Artur je admin, který má plnou kontrolu nad celým bucketem `main`

- Baltazar je uživatel, který by měl vidět a číst pouze soubory s prefixem `s3://main/user=baltazar/`

Údaje které byly obdrženy od CESNET jsou následující:

- storage\_id: `11111111_aaaa_bbbb_cccc_222222222222`

- user\_id admin Artur: `00000aaaaa00000aaaaa00000aaaaa00000aaaaa`

- user\_id uživatel Baltazar: `00000bbbbb00000bbbbb00000bbbbb00000bbbbb`

- access\_key a secret\_key teď nejsou potřeba

V souboru `template_bucket_policy.json` je politika, která obsahuje celkově 3 položky `statement`. První položka statement přiděluje crud práva na celý bucket uživateli Artur. Druhý a třetí statement se starají o to, aby uživatel Baltazar viděl pouze soubory s prefixem `s3://main/user=baltazar/` a měl k nim read práva.

Pro přidání dalšího uživatele je potřeba přidat dvojici statementů, tak jako má uživatel Baltazar. Je potřeba vždy aplikovat kompletní politiku se všemi statementy.

### Pole

#### `Id`

ID bucket politiky. Lze zvolit libovolně.

#### `Version`

Neupravovat a nechat na hodnotě "`2012-10-17`", jiné zatím nejsou podporovány.

#### `Statement.Sid`

ID statementu. Lze zvolit libovolně.

## Lifecycle policy

Lifecycle policy slouží k nastavení časové expirace dat. Po vypršení časové lhůty budou objekty automaticky smazány. Expirace se počítá od data vytvoření objektu.

### Vypsání aplikované lifecycle policy

```shell
aws s3api get-bucket-lifecycle-configuration --endpoint-url https://s3.clX.du.cesnet.cz --bucket {bucket_name}
```

### Aplikace lifecycle policy

```shell
aws s3api put-bucket-lifecycle-configuration --endpoint-url https://s3.clX.du.cesnet.cz --bucket {bucket_name} --lifecycle-configuration file://{path_to_policy}
```

### Příklad

V souboru `lifecycle_policy.json` je příklad kde objekty s prefixem `user=baltazar/` budou automaticky smazány po 365 dnech a objekty s prefixem `user=cecilia/` po 186 dnech.

## Manipulace s daty

S3 je určeno především pro retenci dat a neumožňuje provádění komplexních operací nad daty. Z tohoto důvodu je při provádění analýzy nad daty potřeba data nejprve stáhnout např. na lokální stroj. K tomu slouží klienty zmíněné v předchozích odstavcích. Další možností je nahrání dat do Elasticsearch clusteru. To lze provést dvěma způsoby:

- Stažení dat z S3 na lokální a následný upload dat přes REST API rozhraní Elasticsearch

- Použití nástroje Logstash k jednorázovému přesunu dat z S3 do Elastic.
