# Logstash

- [Instalace](#instalace)
- [Konfigurace](#konfigurace)
- [Pipelines](#pipelines)
- [Index lifecycle management](#index-lifecycle-management)
- [Parsování dat](#parsování-dat)
- [Škálování](#škálování)
- [Nedostatky](#nedostatky)

Logstash je nástroj pro sběr, zpracování a přenos dat z různých zdrojů. Dokáže sbírat data z různých zdrojů, obohatit je a transformovat je pomocí široké škály vstupních, filtrovacích a výstupních pluginů. Základním prvkem konfigurace jsou tzv. pipelines, které jsou popsány níže. Logstash je součástí tzv. ELK stack.

## Instalace

### Prerekvizity

- Ubuntu 22.04
- 16 GB RAM

### Kroky

1. K instalaci doporučujeme použít APT repozitář. Postup lze nalézt v dokumentaci v sekci pro [instalaci pomocí APT](https://www.elastic.co/guide/en/logstash/current/installing-logstash.html#_apt).

1. Aktivovat službu logstash, tak aby se spouštěla automaticky i po restartu serveru.

    ```shell
    systemctl enable logstash.service
    ```

1. Zkopírovat CA certifikát elastic `http_ca.crt` z prvního elastic uzlu do složky `/etc/logstash/`.

1. Upravit konfiguraci v souboru `/etc/logstash/logstash.yml`

### Nastavení Firewallu

Při použití firewallu je potřeba nezapomenou na zpřístupnění těchto portů:

- IN 9600 Logstash API
- IN 5044 Výchozí port pro vstup z beats klientů
- IN 5066 Výchozí port pro metricbeat
- IN 22 SSH

## Konfigurace

- Podrobnější popis konfiguračních souborů je dostupný v [oficiální dokumentaci](<https://www.elastic.co/guide/en/logstash/current/config-setting-files.html>).

- Výchozí hodnoty konfiguračních souborů jsou dostupné v oficiálním [github repozitáři](https://github.com/elastic/logstash/tree/main/config).

### `logstash.yml`

Obecné nastavení logstashe. Viz [dokumentace](https://www.elastic.co/guide/en/logstash/current/logstash-settings-file.html).

- `config.reload.automatic: true`: zařídí aby se znovunačetla konfigurace pipelines při změně konfiguračních souborů bez nutnosti restartu Logstash služby.

- `path.logs: /var/log/logstash`: konfigurace složky pro ukládání logstash logů.

- `path.data: /mnt/data`: buffer pro persistent queues (pokud se používají). Doporučujeme použití rychlého SSD.

- `pipeline.batch.size: 512`: počet událostí které jsou zpracováný zaráz v pipeline.

- `xpack.monitoring.enabled: false`: vypnutí zastaralé metody monitoringu.

Další změny v konfiguraci vyplývají z individuálních potřeb.

### jvm.options

V souboru `jvm.options` se nachází konfigurace JVM. Velikost haldy (heap) by měla optimálně být v rozmezí 4–8 GB. Por Logstash obecně platí, že 16 GB už nepřináší zvýšení výkonu. Viz [dokumentace](https://www.elastic.co/guide/en/logstash/current/jvm-settings.html).

```text
-Xms8g
-Xmx8g
```

### log4j2.properties

Slouží k nastavení vlastností logování logstashe. Lze ponechat ve výchozím nastavení.

### pipelines.yml

V tomto souboru se specifikuje seznam pipelines. V základu stačí jejich název (id) a cesta ke konfiguračnímu souboru.

## Pipelines

Logstash pipelines se skládají ze tří částí:

1. Input: vstup pipeline. Viz [seznam vstupů](https://www.elastic.co/guide/en/logstash/current/input-plugins.html).

2. Filter: zpracování dat která prochází pipeline (filtrování, transformace atd). Viz [seznam filtrů](https://www.elastic.co/guide/en/logstash/current/filter-plugins.html).

3. Output: výstup pipeline. Viz [seznam výstupů](https://www.elastic.co/guide/en/logstash/current/output-plugins.html).

![Obecená struktura pipeline](../img/pipeline-structure.png "Obecená struktura pipeline")

Navržení rozvržení pipelines je velmi individuální záležitost, která závisí na potřebách konkrétního projektu. V tomto repozitáři je přiloženo rozvržení, které bylo použito v rámci projektu. Z tohoto rozvržení lze vycházet.

![Rozvržení pipelines v projektu](../img/pipelines-architecture.jpeg "Rozvržení pipelines v projektu")

**POZN:** položka `id` slouží k identifikaci konkrétního inputu, filtru, outputu. Je důležitá hlavně pro debugging a monitoring. V následujících řádcích je využita pro identifikaci popisovaných částí.

### `in_filebeat.conf`

Tento soubor obsahuje konfiguraci vstupní pipeline pro Beats.

#### `input_beat`

Vstup pipeline který čeká na příchozí beats události. Podporuje jak filebeat tak winlogbeat. Dále je nastaven a cesta k certifikátu.

#### `assign_default_group_name`

V sekci dokumentace pro beats je popsáno přidání pole `[fields][group]`. Pokud však tohle pole není konfigurováno, zařídí tento filtr přidání výchozí hodnoty. Důvodem je použití pole `[fields][group]` v navazujících fázích zpracování dat (pro S3 prefixy a elastic indexy).

#### `copy_hostname` a `remove_hostname_suffix`

Pole `[host][name]` u událostí doručených klientem filebeat nemusí být konzistentní a nelze použít pro unikátní identifikaci stanice. Některé události obsahují hostname se suffixem a některé bez. Proto je potřeba vytvořit pole `[fields][bare_hostname]` u kterého bude zaručena jistota unikátnosti.

Tato kombinace filtrů zařídí vytvoření pole `[fields][bare_hostname]` které bude nést hostname stanice bez suffixů. Seznam suffixů k odstranění je potřeba definovat v seznamu filtru. Shoda se hledá od vrchu dolů (nejpřesnější je potřeba dát nahoru).

#### `output_all`

Využití pipeline-to-pipeline komunikace k odeslání události do vybraných výstupních pipelines.

### `out_s3.conf`

Tento soubor obsahuje konfiguraci výstupní pipeline pro S3 uložiště.

#### `input_p2p_s3`

Pipeline-to-pipeline vstup.

#### `duplicate_winlog_channel_field` a `remove_forward_slashes_from_channel`

Pokud je vyžadováno mít v S3 prefixu hodnotu `channel` je potřeba zařídit, aby tato hodnota neobsahovala znak "`/`" který by v prefixu měl sloužit pro oddělení úrovní. V tomto případě je znak "`/`" nahrazen znakem "`-`".

#### `create_prefix_field_winlog`

Filter pro vytvoření prefixu pro data odeslána winlogem. Příklad výsledného prefixu:
`group=Baltazar-Computers/host=Baltazar-PC/channel=System/year=2015/month=03/day=08/hour=13`

Pozor na interpolaci data. V projektu je použit formát `year=%{+yyyy}`:

- Znak "`+`" znamená, že se použije timestamp události na rozdíl od aktuálního času logstash stroje.

- `yyyy` je očekávaná hodnota roku, pravděpodobně ta kterou chcete použít. `YYYY` je speciální Java formát který počítá s roky po týdnech. Viz rozdíly v Javě. Pozor na záměnu.

#### `create_prefix_field_filebeat`

Stejné jak pro winlog jen bez položky `channel`.

#### `remove_spaces_in_prefix`

Odstraní nepovolené mezery v prefixu a nahradí za znak "`-`".

#### `s3_output`

Zde je potřeba nastavit endpoint, access key a secret key a název bucketu. Bohužel název bucketu nepodporuje interpolaci a musí být nastavena fixní hodnota. (to je důvod proč jsou přístupy do S3 řízen přes prefixy a ne buckety). Dále vysvětlení použitého nastavení:

- `encoding => gzip`: data do S3 budou komprimována do gzip balíčků. Nutno dbát na to, že míra komprese není maximální.

- `rotation_strategy => "size_and_time"`: soubor bude nahrán do S3 při splnění alespoň jedné ze dvou podmínek -> čas sběru a velikost souboru.

- `time_file => 30`: maximální doba sběru jednoho souboru nastavena na 30 minut. Při zvyšování této hodnoty je potřeba si dát pozor na integritu souborů. Už při okně 1h docházelo k špatnému kódování souborů. Byla ovšem vypozorována pouze korelace, nikoliv kauzalita. Tudíž brát pouze jako upozornění na možné problémy.

- `size_file => 33554432`: nastaveno na 32MB, při vyšších hodnotách dosahují rozbalené soubory extrémních velikostí.

- `additional_settings => { force_path_style => true }`: vynutí užívání tzv. _path style_ (namísto _virtual hosted style_), které vyžaduje CESNET úložiště.

### `out_elastic.conf`

Tento soubor obsahuje konfiguraci výstupní pipeline pro elasticsearch cluster.

#### `input_p2p_elastic`

Pipeline-to-pipeline vstup.

#### `remove_original_xml`

Odstranění pole `[event][original]` které je v elasticu redundantní.

#### `elastic_output_with_ilm`

Zde je potřeba nastavit seznam nodů elasticu, API klíč, název indexu a certifikát. Název indexu v základu obsahuje název sbírané skupiny (např. `econ`) a datum. To samé platí i pro ostatní elastic outputs.

Tento output má navíc nastavené rollovaní indexů při dosažení nějaké podmínky. Toto chování je blíže popsáno v sekci [ILM](#index-lifecycle-management). Index má potom tvar `econ-2023-05-01-000001`, kde poslední položka má charakter počítadla.

#### `elastic_output_no_ilm_with_srvc`

Output bez ILM s indexem ve tvaru např. `econ-syslog-2023-05-01`. Index obsahuje informaci o službě.

#### `elastic_output_no_ilm_no_srvc`

Output bez ILM s indexem ve tvaru např. `econ-2023-05-01`.

### `out_tcp.conf`

#### `input_p2p_tcp`

Pipeline-to-pipeline vstup.

#### `tcp_output`

Výstup který využívá standardního TCP protokolu. Pro výstup je potřeba nastavit mód `client`.

### `filter_syslog.conf`

Implementace fitru pro parsování syslog událostí. Filter rozprasuje událost do položek _timestamp_, _host_, _process_, _PID_ a _message_. Kód v této pipeline je ideální vkládat do pipeliny společně s výstupem. Pro lepší orientaci je však v tomto repozitáři oddělen.

## Index lifecycle management

V případě že velikost indexů v elasticsearch pravidelně přesahuje hodnotu 50 GB, je potřeba zavést tzv. index rollover. Tj. mechanismus, který zařídí aby velikost indexu nepřesahovala nějakou hraniční hodnotu (obecně u indexů přesahující 50 GB docházi k degradaci výkonu).

1. Vytvořit lifecycle policy v Kibaně.

2. Vytvořit index template pro požadované index patterny a přiřadit mu lifecycle policy.

3. Vytvořit přes API prázdný počáteční index (tento krok je třeba provést před spuštěním sběru!)

    ```text
    PUT %3Cecon-%7Bnow%2Fd%7Byyyy-MM-dd%7D%7D-000001%3E
    {
      "aliases": {
        "myrolloveralias": {
          "is_write_index": true
        }
      }
    }
    ```

    `<indexname-{now/d{yyyy-MM-dd}}-000001>` je potřeba zakodovat přes URL encoding.

4. Nastavit output v logstashi

    ```text
    ilm_rollover_alias => "econ"
    ilm_pattern => "{now/d{yyyy-MM-dd}}-000001"
    ilm_policy => "econ-ilc"
    ilm_enabled => true
    ```

5. Spustit sběr

## Parsování dat

V rámci projektu byly prozkoumány a vyzkoušeny následující možnosti parsování dat:

- Parsování pomocí Logstash filtrů (doporučeno)

  - ➕: Flexibilita. Nejširší nabídka operací. Možnost volby různé úrovně zpracování pro různé výstupy. Škálování nasazením další instance Logstashe. Velká kontrola nad daty.

  - ➖: Časová náročnost tvorby filtrů.

- Parsování pomocí filebeat [modulů](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-modules.html)

  - ➕: Jednoduché zprovoznění (u služeb pro které existuje modul). Předpřipravené dashboardy a vizualizace.

  - ➖: Náročné psaní vlastních modulů. Omezený výběr služeb.

- Parsování pomocí ingest pipelines na straně Elasticsearch clusteru

  - ➕: Škálování nasazením dalšího ingest uzlu.

  - ➖: Parsování probíhá těsně před indexací do Elasticsearch, nelze tedy parsovaná data ukládat jinam. Při větších objemech je nutné dedikovat speciální ingest uzel. Nižší podpora operací a výkon než u Logstashe.

- Parsování na straně filebeat (nedoporučeno)

  - ➕: Rozložení zátěže na klienty.

  - ➖: Velmi omezená množina operací, které je možné provádět nad daty. Problematické nasazení a údržba, je potřeba aktualizovat konfiguraci na všech klientských stanicích.

Možnosti lze mezi sebou kombinovat, např. použitím modulů (které vnitřně využívají ingest pipelines) společně s ingest pipelines. Z důvodu vyšší flexibility a kontrolu nad daty bylo v projektu zvoleno parsování na straně Logstashe. Příklad posloupnosti filtrů pro parsování syslog zpráv je uveden v souboru `filter_syslog.conf`.

**POZN:** Parsování je nutné řešit pouze při použití klienta filebeat. Winlogbeat parsuje windows logy již ve výchozím nastavení.

## Škálování

Pokud jedna instance Logstashe nestačí a dochází k propadům ve výkonu, lze řešení škálovat nasazením dalších instancí. Tyto instance běží nezávisle na sebe (netvoří cluster), ale pro uchování integrity dat je důležité zajistit, aby všechny instance běžely se stejnou konfigurací! Zátěž lze rozložit mezi jednotlivé instance následujícími způsoby:

- V konfiguraci beats v nastavení pro output logstashe specifikovat všechny instance. Beats protokol se pak sám stará o zajištění rozložení zátěže a zaručí doručitelnost. Nevýhodou je nemožnost použití tohoto řešení s jinými vstupy.

- DNS load balancer. Výhodou je odstínění klientů od load balancingu, při přidání logstash stroje, není potřeba překonfigurovat všechny beats klienty.

- Hardwarový load balancer. Stejná výhoda jako DNS load balancer + rychlost.

## Nedostatky

### Backpressure, blokování a záruka doručení

Pokud je některý z --výstupů nepřístupný--, bude příslušná output pipeline zablokována a bude tvořit --backpressure-- na předchozí pipelines nebo vstupy. Tento backpressure se propaguje až k --beatsům--, které dočasně --pozastaví odesílání-- nových událostí. Datový tok se obnoví až ve chvíli, kdy je výstup opět dostupný. Beats zpětně odešle veškeré události, které nebyly ještě doručeny. Tzn. je --zajištěno-- že se všechny --události budou doručeny alespoň jednou-- na všechny výstupy.

Tenhle mechanismus je jedna ze stěžejních výhod použití kombinace Beats+Logstash. Obnáší s sebou však i velkou nevýhodu: v situaci kdy dojde k výpadku jednoho z výstupů, pozastaví se přijímaní dat na příslušném vstupu/ech a tím pádem k pozastavení ostatních výstupů (pokud tam je závislost na stejném vstupním zdroji).

#### Řešení problému

1. Zavedením --pipeline-to-pipeline-- komunikace --přes-- protokol --UDP--, který nepropaguje back pressure. Tohle řešení nezabrání ztrátě dat na nedostupném výstupu, ale zajistí že se zbytek datového flow nepozastaví.

2. Pečlivým --monitoringem-- všech --výstupů-- a rychlým řešením případných výpadků. Žádné události nebudou zahozeny (pokud nebudou v mezičase vymazány z koncových stanic), ale nikdy nebude zajištěn 100% real-time flow.

3. Zavedení --persistent queues--. PQ slouží jako buffer pro pipeline, který ukládá údálosti na pevný disk. Pří výpadku výstupu jsou události ukládány do PQ a předejde se okamžitému zablokování. K zablokování dojde při vyčerpání kapacity PQ. Tohle řešení velmi výrazně snižuje výkon Logstashe a navíc řeší pouze dočasný výpadek (kapacita PQ není nekonečná). Z tohoto důvodu nejsou PQ v projektu použity. Viz dokumentace k [PQ](https://www.elastic.co/guide/en/logstash/current/persistent-queues.html).

### S3 output plugin - limit file descriptorů

Při použití S3 output pluginu pro logstash je potřeba myslet na skutečnost, že každý souboru s unikátním prefixem sbíraný a ukládaný na S3 otevírá vlastní file descriptor. Výchozí limit otevřených file descriptorů v ubuntu 22.04 je pro služby asi 16K.

#### Vznik problému

Uvažujme situaci, kdy se sbírají logy z windows stanic a užívá se prefix `/host=%{[host][hostname]}/year=%{+yyyy}/month=%{+MM}/day=%{+dd}/hour=%{+HH}`. Sběr se pustí souběžně na 1000 stanicích a bude se uvažovat 72h historie. Na straně logstashe se tedy bude pro každý unikátní prefix vytvářet soubor, do kterého se budou data buffrovat:

- `/host=Antonio/year=2023/month=06/day=01/hour=00/file.txt.gz`

- `/host=Antonio/year=2023/month=06/day=01/hour=01/file.txt.gz`

- `/host=Antonio/year=2023/month=06/day=01/hour=02/file.txt.gz`

- …

Celkově takových kombinací může být v tomto případě nárazově až 72K.

#### Řešení problému

1. Navýšení limitu otevřených file descriptorů pro službu logstash.service

    1. Najít konfigurační soubor logstash služby, v ubuntu: `/usr/lib/systemd/system/logstash.service`

    2. Najít řádek `LimitNOFILE=16384` a nastavit novou vyšší hodnotu

    3. Restartovat systemd a logstash službu

        ```shell
        systemctl daemon-reexec
        systemctl restart logstash
        ```

2. Snížení granularity prefixu

3. Vyvarování se nasazení velkého počtu stanic naráz

Dále je možné monitorovat počet otevřených file desriptorů na endpointu logstash API: `http://{logstash_ip}:9600/_node/stats/process?pretty`

### Velký objem logů (zápisů logstashe do syslog)

V ojedinělých případech může nastat situace, kdy logstash generuje velké množství událostí do syslogu. Typicky se jedná o situaci, kdy se ve filtrech nachází chyba, potom se pro každou příchozí událost zpracovanou logstashem vygeneruje jedna syslog událost na hostitelském stroji. Pokud je objem zpracovaných logů velký, může po určitém čase dojít k zaplnění diskové kapacity.

#### Řešení problému

1. Pečlivá kontrola nasazené konfigurace (hlavně filtrů, podívat se do logů jestli se negeneruje mnoho chyb)

2. Nastavení limitu velikosti a počtu uchovaných logů na úrovni system

    1. Nalézt konfigurační soubor logrotate pro syslog. V ubuntu typicky: `/etc/logrotate.d/rsyslog`

    2. Přidat nebo upravit následující řádky. Uzpůsobit podle vlastních potřeb.

        ```text
        {
          rotate 5
          size 5G
          weekly
        }
        ```

        `Rotate` udává počet starých rotovaných souborů. `Size` udává maximální velikost jednoho rotovaného souboru. `Weekly` maximální doba po které dojde k rotaci. Tohle nastavení zařídí, aby se velikost uchovaných logů nepřesáhl (5+1)\-5GB = 30GB.

### Connection reset warning

Při odpojení filebeat nebo winlogbeat klienta od logstashe dochází k vytvoření události `java.net.SocketException: Connection reset` na straně Logstash, která je zapsána do syslog. Tohle upozornění není nutně chyba, jelikož k vytvoření události dochází i při správném ukončení beats klienta. Z tohoto důvodu lze ve většině případů upozornění ignorovat.

#### Řešení problému

Tohle řešení není doporučeno, jelikož může dojít k potlačení jiných důležitých událostí. V případě přijetí tohoto rizika stačí upravit nastavení v souboru `log4j2.properties` a to konkrétně změněním úrovně logování následujících loggerů:

```text
logger.beatsinput.level = off
logger.io.netty.channel.DefaultChannelPipeline.level = off
```

### Problém s přístupovými právy k souborům

V případě že se nedaří spustit logstash službu kvůli problému s právy k souborům (v lozích bude zmíněno klíčové slovo _permission_) je potřeba nastavit vlastnictví potřebných souborů uživateli `logstash`. K těmto chybám dochází když je logstash spuštně přímo pomocí binary souboru namísto spuštění standardní služby.

#### Řešení problému

```shell
sudo chown --recursive logstash /tmp/logstash
sudo chgrp --recursive logstash /tmp/logstash

sudo chown --recursive logstash /var/log/logstash
sudo chgrp --recursive logstash /var/log/logstash

sudo chown --recursive logstash /etc/logstash
sudo chgrp --recursive logstash /etc/logstash
```
