# Artefakty projektu Datalake

## Základní informace

### Název projektu

Vytvoření datového jezera pro potřeby uchovávání provozních dat Masarykovy univerzity

### Řešitelé

- Ing. Jindřich Zechmeister (MU)
- RNDr. Daniel Tovarňák, Ph.D. (MU)
- Mgr. Martin Kotlík (MU)
- Bc. Matúš Gajdoš (MU)
- Bc. Andrea Chimenti (VUT)

### Období

29\. 6\. 2022 - 29\. 6\. 2023

### URL

<https://fondrozvoje.cesnet.cz/projekt.aspx?ID=690>

## Obsah repozitáře

Tento repozitář obsahuje výstupní artefakty projektu. Jednotlivé technologie jsou rozděleny po složkách s následující strukturou:

```text
├── Beats
│   ├── Filebeat
│   └── Winlogbeat
├── Elasticsearch_Kibana
├── Logstash
├── StackMonitor
└── S3
```

Složka `img` obsahuje pomocné obrázky do readme souborů.
