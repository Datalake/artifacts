$beatsRootDirectory = 'C:/Program Files/Beats'

# Winlogbeat
Stop-Service -Name winlogbeat
&(Join-Path $beatsRootDirectory "Winlogbeat/uninstall-service-winlogbeat.ps1")
Remove-Item -Path "$beatsRootDirectory/Winlogbeat" -Recurse

# Global
Remove-Item -Path "$beatsRootDirectory" -Recurse