# Winlogbeat

Winlogbeat je open-source nástroj vyvinutý společností Elastic, který slouží k sběru a posílání událostí z Windows událostního záznamu (Windows Event Log) do Elastic Stacku. Jedná se o součást Beats, který je kolekcí lehkých datových sběračů, které shromažďují různé typy dat a posílají je do Elastic Stacku pro analýzu, vizualizaci a ukládání.

## Instalace

- Instalátor pro Windows lze stáhnout z [oficálních stránek](https://www.elastic.co/downloads/beats/winlogbeat). V tomto repozitáři se nachází námi rozšířená verze, kterou doporučujeme použít.

- V projektu byla použita verze ZIP, protože verze MSI instalátoru byla v době psaní v betě a nebylo možné se spolehnout na bezproblémovou funkčnost.

### Kroky

1. Instalace probíhá přes Powershell skript `install\_beats.ps1`.

1. Skript nakopíruje potřebné soubory do složky `C:/Program Files/Beats`.
    - Konfigurace serverů: `winlogbeatSRV.yml`.
    - Konfigurace pracovních stanic: `winlogbeatWS.yml`.
    - V souboru `http_ca.crt` se používá certifikát od CA, který je umístěn na Logstash serverech.

1. Podle typu zařízení Server/Stanice použije script specifický konfigurační soubor

    - Liší se podle odesílaných event logů a taky podle jména v poli `fields.groups`  
    - V konfiguraci se sbírají základní Windows eventlogy System, Security, System, Setup a Microsoft-Windows-Windows Defender/Operational

1. Winlogbeat bude nakonfigurován jako služba, která se automaticky spouští po spuštění počítače.

![List of Windows services with Winlogbeat](../../img/services-windows.png)

### Hromadná instalace

Souběžnou instalaci na více zařízení lze provést přes SCCM konzoli.

1. Zařízení, které chceme, aby odesílalo logy, přidáme do kolekce \\Assets and Compliance\\Overview\\Device Collections\\Workstations\\WKS - Software Distribution\\WKS - SD - Applications\\WKS - SD - Elastic Beats - Required - Admin only.

2. Všechny kroky z předchozího bodu Instalace se již provedou samy a automaticky.

3. Pokud chceme logování zase odebrat, tak dáme stanici, nebo server z kolekce pryč.

## Odinstalace Winlogbeats

Odinstalace Winlogbeats probíhá přes stejný instalační balíček skriptem **uninstall\_beats.ps1**.
