$beatsRootDirectory = 'C:/Program Files/Beats'
$RegPath = "HKLM:\Software\Beats"
$beatsVersion = "8.5.2"
$beatsConfig = "1"

# Uninstall winlogbeat if installed
if (Get-Service winlogbeat -ErrorAction SilentlyContinue) {
    $service = Get-WmiObject -Class Win32_Service -Filter "name='winlogbeat'"
    $service.StopService()
    Start-Sleep -s 1
    $service.delete()
    Remove-Item -Path "$beatsRootDirectory/Winlogbeat" -Recurse
}

# Uninstall metricbeat if installed
if (Get-Service metricbeat -ErrorAction SilentlyContinue) {
    $service = Get-WmiObject -Class Win32_Service -Filter "name='metricbeat'"
    $service.StopService()
    Start-Sleep -s 1
    $service.delete()
    Remove-Item -Path "$beatsRootDirectory/Metricbeat" -Recurse
}

# Uninstall filebeat if installed
if (Get-Service filebeat -ErrorAction SilentlyContinue) {
  $service = Get-WmiObject -Class Win32_Service -Filter "name='filebeat'"
  $service.StopService()
  Start-Sleep -s 1
  $service.delete()
  Remove-Item -Path "$beatsRootDirectory/Filebeat" -Recurse
}

# Global
if( -Not (Test-Path -Path $beatsRootDirectory) )
{
    New-Item -ItemType directory -Path $beatsRootDirectory
}

# Winlogbeat
Copy-Item -Path ".\Winlogbeat" -Destination ( Join-Path $beatsRootDirectory "/") -Recurse
&(Join-Path $beatsRootDirectory "Winlogbeat/install-service-winlogbeat.ps1")
Start-Service -Name winlogbeat

# Write winlogbeat version and config version to registry

try{  
  Get-ItemProperty -Path $RegPath -Name "Beats_version" -ErrorAction Stop
  Get-ItemProperty -Path $RegPath -Name "Beats_config" -ErrorAction Stop
}  
catch [System.Management.Automation.ItemNotFoundException] {  
  New-Item -Path $RegPath -Force  
  New-ItemProperty -Path $RegPath -Name "Beats_version" -Value $beatsVersion -Force
  New-ItemProperty -Path $RegPath -Name "Beats_config" -Value $beatsConfig -Force
}  
catch {  
  New-ItemProperty -Path $RegPath -Name "Beats_version" -Value $beatsVersion -Type String -Force
  New-ItemProperty -Path $RegPath -Name "Beats_config" -Value $beatsConfig -Type String -Force
}  