# Filebeat

Filebeat je lightweight nástroj, který slouží primárně ke sběru logů na klientských stanicích a jejich odeslání na Logstash. Jeho výhodou je garantované doručení zpráv i po výpadku sítě, Logstashe apod. a jednoduchá konfigurace. Filebeat běží jako služba pod systemd. Podrobnější popis toho, jak Filebeat funguje, je dostupný v [dokumentaci výrobce](https://www.elastic.co/guide/en/beats/filebeat/8.6/how-filebeat-works.html#_how_does_filebeat_keep_the_state_of_files). Ve zkratce: pamatuje si polohu v souborech a sleduje inkrementální změny.

Sběr se nastavuje definicí cest k log souborům v souboru `filebeat.yml` v sekci `filestream.inputs` (viz [filestream input](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-filestream.html)). Parsování dat bude obstaráno logstashem. Jednotlivé vstupy je vhodné označit označit polem `service`, které bude využito pro volby parsovacích filterů v logstashi a pro název indexu. Také je možné přidat tagy pro upřesnění vstupu. Tahle pole nedoporučujeme vynechávat protože nebude možná identifikace dat na straně Logstashe a v úložišti bude vznikat datová bažina.

## Instalace

V repozitáři jsou přiloženy 2 konfigurační soubory

- `filebeat.yml`: Naše šablona pro zjednodušené nasazení. Obsahuje popis všech námi použitých polí.
- `filebeat.reference.yml`: Referenční soubor z oficiální dokumentace. Obsahuje veškeré nastavení i s popisem.

### Kroky

1. Instalaci balíčku je možno provést přes DPKG, RPM, APT nebo YUM (preferujte apt a yum). Viz bod 1 v [dokumentaci](https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation-configuration.html#installation).

2. Pozastavit upgrade beatů např. pomocí `sudo apt-mark hold filebeat`. Aktualizace klientů by měla probíhat zároveň s aktualizací ostatních komponent ELK.

3. Nastavit konfigurační soubor `/etc/filebeat/filebeat.yml`. Doporučujeme vycházet z námi vytvořené šablony.

    1. Položka `filebeat.inputs.paths`, určuje které soubory budou sledovány.

    2. Položku `fields.group` je potřeba nastavit na název pracovní skupiny tj. kdo sbírá dat. Slouží k identifikaci.

    3. Položku `fields.os` je potřeba nastavit na hodnotu `linux` nebo `windows`. (Filebeat v defaultu neodesílá informaci o os a může být nasazen i na Windows)

    4. Případně změnit cestu k certifikátu.

4. Nahrát certifikát na server/stanici, v šabloně nastaven název `http_ca.crt`.

5. Ověřit že je port 5044 otevřen a není blokován firewallem apod.

6. Příkazem `sudo systemctl enable filebeat` nastavit službu filebeat, aby se sama spouštěla po restartu systému. Nebo možno jednorázově spustit příkazem `start`.

**Poznámka k rotaci logů**: Prosím zkontrolovat že pro rotaci logů není použita strategie `copytruncate`. Viz zmínka v [dokumentaci](https://www.elastic.co/guide/en/beats/filebeat/current/file-log-rotation.html).

### Troubleshooting

Pro kontrolu správnosti konfiguračního souboru a nastaveného outputu lze použít následující příkazy:

```text
sudo filebeat test config
sudo filebeat test output
```
