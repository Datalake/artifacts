# Internal monitoring with Metricbeat

![Metricbeat scheme](../img/metricbeat-scheme.png)

## Install metricbeat

1. Follow official [documentation](https://www.elastic.co/guide/en/beats/metricbeat/current/metricbeat-installation-configuration.html).

    ```shell
    curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-8.8.1-amd64.deb
    sudo dpkg -i metricbeat-8.8.1-amd64.deb
    ```

1. Configure to elastic output in metricbeat.yml

    ```yml
    output.elasticsearch:
      hosts: ["https://<e1-ip>:9200", "https://<e2-ip>:9200", etc.]
      username: "remote_monitoring_user"
      password: "YOUR_PASSWORD" 
      ssl:
        enabled: true
        ca_trusted_fingerprint: "es_fingerprint"
    ```

1. Using API set x-pack monitoring

    ```text
    GET _cluster/settings

    PUT _cluster/settings
    {
        "persistent": {
            "xpack.monitoring.collection.enabled": true
        }
    }
    ```

### Elastic nodes

1. Enable elasticsearch module

    ```shell
    metricbeat modules enable elasticsearch-xpack
    metricbeat modules disable system
    ```

### Kibana nodes

1. disable default collection

    ```yml
    monitoring.kibana.collection.enabled: false
    ```

1. setup metricbeat modules

    ```shell
    metricbeat modules enable kibana-xpack
    metricbeat modules disable system
    ```

1. Configure the Kibana X-Pack module in Metricbeat.
  Set the following settings in file `/etc/metricbeat/modules.d/kibana-xpack.yml`:

    ```yml
    - module: kibana
      metricsets:
        - stats
      period: 10s
      hosts: ["localhost:5601"]
      #basepath: ""
      #username: "user"
      #password: "secret"
      xpack.enabled: true
    ```

### Logstash nodes

1. disable logstash default metric collection in `/etc/logstash/logstash.yml`

    ```yml
    monitoring.enabled: false
    ```

1. enable logstash module

    ```shell
    metricbeat modules enable logstash-xpack
    metricbeat modules disable system
    ```

1. Configure the logstash-xpack module in Metricbeat.
  Set the following settings in file `/etc/metricbeat/modules.d/logstash-xpack.yml`:

    ```yml
    - module: logstash
      metricsets:
        - node
        - node_stats
      period: 10s
      hosts: ["localhost:9600"]
      #username: "user"
      #password: "secret"
      xpack.enabled: true
    ```

1. start metricbeat on each node

    ```shell
    sudo systemctl enable metricbeat
    sudo systemctl start metricbeat
    ```

## Result in Kibana

![Kibana monitoring](../img/kibana-monitoring.png)
