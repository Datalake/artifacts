# Elasticsearch & Kibana

## Install elaticsearch nodes

Use  [installation guide](<https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html>) for current version.

### Import PGP key

```shell
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
```

### Install repository

```shell
sudo apt-get install apt-transport-https

echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list
```

### Install elasticsearch

1. Install package

    ```shell
    sudo apt-get update && sudo apt-get install elasticsearch
    ```

1. Save following output

    ```text
    -------Security autoconfiguration information-------

    Authentication and authorization are enabled.
    TLS for the transport and HTTP layers is enabled and configured.

    The generated password for the elastic built-in superuser is : <password>

    If this node should join an existing cluster, you can reconfigure this with
    '/usr/share/elasticsearch/bin/elasticsearch-reconfigure-node --enrollment-token <token>'
    after creating an enrollment token on your existing cluster.

    You can complete the following actions at any time:

    Reset the password of the elastic built-in superuser with
    '/usr/share/elasticsearch/bin/elasticsearch-reset-password -u elastic'.

    Generate an enrollment token for Kibana instances with
    '/usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s kibana'.

    Generate an enrollment token for Elasticsearch nodes with
    '/usr/share/elasticsearch/bin/elastic
    ```

1. On all elasticsearch nodes change configuration in /etc/elasticsearch/elasticsearch.yml to:

    ```text
    node.name: ${HOSTNAME}
    cluster.name: elk-muni
    network.host: _ens160:ipv4_
    path.data: /mnt/data
    ```

1. On nodes e1, e2 and e3 in `/etc/elasticsearch/elasticsearch.yml` add:

    ```text
    node.attr.data: hot
    ```

1. On nodes e4 and e5 in  /etc/elasticsearch/elasticsearch.yml add:

    ```text
    node.attr.data: warm
    ```

1. Allow firewall ports

    ```shell
    sudo ufw allow from XXX.XXX.XXX.0/24 to any port 9300
    sudo ufw allow from XXX.XXX.XXX.0/24 to any port 9200
    sudo ufw status
    ```

### Configure memory locking

[Configuring system settings | Elasticsearch Guide \[8.8\] | Elastic](https://www.elastic.co/guide/en/elasticsearch/reference/8.8/setting-system-settings.html#systemd)

As root on all elasticsearch nodes run:

```shell
vi /etc/security/limits.conf
elasticsearch soft memlock unlimited
elasticsearch hard memlock unlimited

sudo swapoff -a
sysctl -w vm.max_map_count=262144
```

Otherwise Elasticsearch won't work properly.

### Start first node

Run on first node:

```shell
sudo systemctl enable elasticsearch
sudo systemctl start elasticsearch
```

### Generate enrollment token

In case that the token from installation output was not saved, it needs to be re-genarated.

```shell
/usr/share/elasticsearch/bin/elasticsearch-create-enrollment-token -s node
```

Save the output

### Other nodes

1. Use the enrollment token from first node.

    ```shell
    /usr/share/elasticsearch/bin/elasticsearch-reconfigure-node --enrollment-token <enrollment-token>
    ```

    Save the output

1. Start node

    ```shell
    sudo systemctl start elasticsearch
    ```

1. Test node and cluster health

    ```shell
    curl --insecure -X GET "https://<master-node-ip>:9200/_cluster/health?pretty"
    ```

1. check all nodes configuration looks like:

    ```yml
    discovery.seed_hosts: ["e1", "e2","e3","e4","e5"]
    cluster.initial_master_nodes: ["e1","e2","e3"]
    ```

    ```yml
    node.roles: [ master, data, remote_cluster_client, ingest ]
    # Enable security features
    xpack.security.enabled: true

    xpack.security.enrollment.enabled: true

    # Enable encryption for HTTP API client connections, such as Kibana, Logstash, and Agents
    xpack.security.http.ssl:
    enabled: true
    keystore.path: certs/http.p12

    # Enable encryption and mutual authentication between cluster nodes
    xpack.security.transport.ssl:
    enabled: true
    verification_mode: certificate
    keystore.path: certs/transport.p12
    truststore.path: certs/transport.p12


    # Allow HTTP API connections from anywhere
    # Connections are encrypted and require user authentication
    http.host: 0.0.0.0


    ## Allow other nodes to join the cluster from anywhere
    # Connections are encrypted and mutually authenticated
    #transport.host: 0.0.0.0
    ```

## Install Kibana

1. Follow [Install Kibana | Kibana Guide \[8.8\] | Elastic](https://www.elastic.co/guide/en/kibana/current/install.html)

    ```shell
    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elasticsearch-keyring.gpg
    sudo apt-get install apt-transport-https

    echo "deb [signed-by=/usr/share/keyrings/elasticsearch-keyring.gpg] https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee /etc/apt/sources.list.d/elastic-8.x.list

    sudo apt-get update && sudo apt-get install kibana

    sudo /bin/systemctl daemon-reload
    sudo /bin/systemctl enable kibana.service

    sudo ufw allow 5601
    sudo ufw status
    sudo ufw enable
    sudo ufw disable
    ```

1. Generate certificates and key for Kibana webconsole using your CA and copy them to `/etc/kibana/certs/`

1. Setup kibana
  sudo vi /etc/kibana/kibana.yml

    ```yml
    server.host: "<kibana-server-ip>"
    server.port: 443
    server.ssl.enabled: true
    server.ssl.certificate: /etc/kibana/certs/logs.ics.muni.cz.crt
    server.ssl.key: /etc/kibana/certs/logs.ics.muni.cz.key
    elasticsearch.hosts: ['https://<e1-ip>:9200', 'https://<e2-ip>:9200','https://<e3-ip>:9200','https://<e4-ip>:9200','https://<e5-ip>:9200']
    ```

1. Start kibana

    ```shell
    systemctl start kibana
    ```

1. On elastic node generate kibana enrollment token

    ```shell
    /usr/share/elastic/bin/elasticsearch-create-enrollment-token --scope kibana
    ```

1. Access `https://<kibana-ip>` via web browser and put token there:
  ![Enter enrollment token](../img/kibana_enrollment.png)<!-- {"width":500} -->

1. On kibana node generate verification code

    ```shell
    /usr/share/kibana/bin/kibana-verification-code
    ```

1. COPY/PASTE code to kibana web interface
  ![Kibana paste code](../img/kibana-code.png)<!-- {"width":500} -->

1. On Ubutu allow port 443 to be used for kibana. Run the following commands as root:

    ```shell
    setcap cap_net_bind_service=+epi /usr/share/kibana/bin/kibana
    setcap cap_net_bind_service=+epi /usr/share/kibana/bin/kibana-plugin
    setcap cap_net_bind_service=+epi /usr/share/kibana/bin/kibana-keystore
    setcap cap_net_bind_service=+epi /usr/share/kibana/node/bin/node
    ```

## APIs

Monitor and manage elastic from commandline or API. You can also run `curl` on localhost.

### Health

```shell
curl --insecure -X GET "https://<e1-ip>:9200/_cluster/health?pretty"
```

Output:

```text
{
    "cluster_name" : "elk-muni",
    "status" : "green",
    "timed_out" : false,
    "number_of_nodes" : 5,
    "number_of_data_nodes" : 5,
    "active_primary_shards" : 443,
    "active_shards" : 886,
    "relocating_shards" : 0,
    "initializing_shards" : 0,
    "unassigned_shards" : 0,
    "delayed_unassigned_shards" : 0,
    "number_of_pending_tasks" : 0,
    "number_of_in_flight_fetch" : 0,
    "task_max_waiting_in_queue_millis" : 0,
    "active_shards_percent_as_number" : 100.0
}
```

Example of APIs calls:

| function             | endpoint                            |
|----------------------|-------------------------------------|
| current master       | /_cat/master?v                      |
| indices              | /_cat/indices?v                     |
| cluster settings     | /_cluster/settings                  |
| node simple overview | /_cat/nodes?v&h=ip,name,role,master |
| shard info           | /_cat/shards?v?h=index, shard       |

### Create API key

Example for logstash writer role

1. create logstash writer role

    ```json
    POST /_security/api_key
    {
        "name": "logstash_api", 
        "role_descriptors": {
            "logstash_api": { 
                "cluster": [
                    "manage_index_templates",
                    "monitor",
                    "manage_ilm",
                    "manage_logstash_pipelines",
                    "cluster:admin/xpack/monitoring/bulk"
                ],
                "index": [
                    {
                        "names": ["*"],
                        "privileges": ["all"]
                    }
                ]
            }
        }
    }
    ```

    Response:

    ```json
    {
        "id": "yyy",
        "name": "logstash_api",
        "api_key": "xxx",
        "encoded": "zzz"
    }
    ```

1. in  logstash or api calls use format  "`id:api_key`":

    ```text
    yyy:xxx
    ```
